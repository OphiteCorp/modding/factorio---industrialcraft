data:extend({
	-- inserters
	{
		type = "int-setting",
		name = "ic-tweaks-inserter-max-stack-size",
		order = "inserter-a",
		setting_type = "startup",
		default_value = 4,
		minimum_value = -1,
		maximum_value = 100
	},
	-- modules
	{
		type = "int-setting",
		name = "ic-tweaks-module-slots-mult",
		order = "module-a",
		setting_type = "startup",
		default_value = 2,
		minimum_value = 1,
		maximum_value = 20
	},
	{
		type = "bool-setting",
		name = "ic-tweaks-no-modules-restrictions",
		order = "module-b",
		setting_type = "startup",
		default_value = true
	},
	{
		type = "bool-setting",
		name = "ic-tweaks-remove-module-speed-penalty",
		order = "module-c",
		setting_type = "startup",
		default_value = true
	},
	{
		type = "bool-setting",
		name = "ic-tweaks-remove-module-quality-penalty",
		order = "module-d",
		setting_type = "startup",
		default_value = true
	},
	{
		type = "double-setting",
		name = "ic-tweaks-module-speed-mult",
		order = "module-e",
		setting_type = "startup",
		default_value = 1,
		minimum_value = 0.1,
		maximum_value = 100
	},
	{
		type = "double-setting",
		name = "ic-tweaks-module-consumption-mult",
		order = "module-f",
		setting_type = "startup",
		default_value = 1,
		minimum_value = 0.1,
		maximum_value = 100
	},
	{
		type = "double-setting",
		name = "ic-tweaks-module-pollution-mult",
		order = "module-g",
		setting_type = "startup",
		default_value = 1,
		minimum_value = 0.1,
		maximum_value = 100
	},
	{
		type = "double-setting",
		name = "ic-tweaks-module-productivity-mult",
		order = "module-h",
		setting_type = "startup",
		default_value = 1,
		minimum_value = 0.1,
		maximum_value = 100
	},
	{
		type = "double-setting",
		name = "ic-tweaks-module-quality-mult",
		order = "module-i",
		setting_type = "startup",
		default_value = 1,
		minimum_value = 0.1,
		maximum_value = 100
	},
	-- majak
	{
		type = "double-setting",
		name = "ic-tweaks-beacon-boost-mult",
		order = "beacon-a",
		setting_type = "startup",
		default_value = 0.5,
		minimum_value = 0,
		maximum_value = 100
	},
	{
		type = "double-setting",
		name = "ic-tweaks-beacon-area-mult",
		order = "beacon-b",
		setting_type = "startup",
		default_value = 1.5,
		minimum_value = 0.1,
		maximum_value = 100
	},
	{
		type = "bool-setting",
		name = "ic-tweaks-colored-beacons-on-modules",
		order = "beacon-c",
		setting_type = "startup",
		default_value = true
	},
	-- recipe
	{
		type = "bool-setting",
		name = "ic-tweaks-remove-productivity-recipe-cap",
		order = "recipe-a",
		setting_type = "startup",
		default_value = true
	},
	-- fluids
	{
		type = "int-setting",
		name = "ic-tweaks-fluid-volume-mult",
		order = "fluid-a",
		setting_type = "startup",
		default_value = 1,
		minimum_value = 1,
		maximum_value = 1000000
	},
	{
		type = "string-setting",
		name = "ic-tweaks-fluid-volume-entities",
		order = "fluid-b",
		setting_type = "startup",
		default_value = "pipe, pipe-to-ground, infinity-pipe",
		allow_blank = true
	},
	-- others
	{
		type = "bool-setting",
		name = "ic-tweaks-allow-chests-in-space",
		order = "other-a",
		setting_type = "startup",
		default_value = true
	},
	{
		type = "int-setting",
		name = "ic-tweaks-inventory-size",
		order = "other-b",
		setting_type = "startup",
		default_value = 200,
		minimum_value = -1,
		maximum_value = 9999999
	},
	{
		type = "double-setting",
		name = "ic-tweaks-belt-speed-multiplier",
		order = "other-c",
		setting_type = "startup",
		default_value = 1,
		minimum_value = 0.1,
		maximum_value = 100
	},
	{
		type = "int-setting",
		name = "ic-tweaks-min-stack-size",
		order = "other-d",
		setting_type = "startup",
		default_value = 500,
		minimum_value = 1,
		maximum_value = 2000000000
	},
	{
		type = "bool-setting",
		name = "ic-tweaks-construction-restrictions",
		order = "other-e",
		setting_type = "startup",
		default_value = false
	}
})
