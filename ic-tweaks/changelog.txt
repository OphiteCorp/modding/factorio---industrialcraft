---------------------------------------------------------------------------------------------------
Version: 1.1.2
Date: 2024. 12. 20
  Changes:
	- Option to disable some configurations
---------------------------------------------------------------------------------------------------
Version: 1.1.1
Date: 2024. 12. 13
  Changes:
	- Added flow multiplier in pipes
	- Ability to build everything on all planets without restrictions
---------------------------------------------------------------------------------------------------
Version: 1.1.0
Date: 2024. 11. 17
  Changes:
	- Initial commit