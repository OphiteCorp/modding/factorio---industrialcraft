# Tweaks

Různá nastavení, které se nemusí týkat žádného konkrétního módu. Pár příkladů:

- Větší počet slotů v inventáři
- Více modul slotů v budovách
- Různé násobice pro modul sloty, kvality, rychlosti pásů atd.
- Úprava majáku
- Změna velikosti stacku předmětů
- Změna průtoku kapalin v trubkách
- Možnost stavět všechno na všech planetách bez omezení
- a mnoho dalšího...
