local inserter_max_stack_size = settings.startup["ic-tweaks-inserter-max-stack-size"].value
local module_slots_mult = settings.startup["ic-tweaks-module-slots-mult"].value
local beacon_boost_mult = settings.startup["ic-tweaks-beacon-boost-mult"].value
local beacon_area_mult = settings.startup["ic-tweaks-beacon-area-mult"].value
local no_modules_restrictions = settings.startup["ic-tweaks-no-modules-restrictions"].value
local min_stack_size = settings.startup["ic-tweaks-min-stack-size"].value
local fluid_volume_mult = settings.startup['ic-tweaks-fluid-volume-mult'].value
local fluid_volume_entities = settings.startup['ic-tweaks-fluid-volume-entities'].value
local construction_restrictions = settings.startup['ic-tweaks-construction-restrictions'].value

----------------------------------------------------------------------------------------------------------------------------------

local ic = {}

function ic.trim(str)
    return str:gsub("^%s*(.-)%s*$", "%1")
end

function ic.split_with_comma(str)
    local fields = {}
    for field in str:gmatch('([^,]+)') do
        fields[#fields + 1] = ic.trim(field)
    end
    return fields
end

function ic.has_value(data, value)
    for _, v in ipairs(data) do
        if v == value then
            return true
        end
    end
    return false
end

fluid_volume_entities = ic.split_with_comma(fluid_volume_entities)

----------------------------------------------------------------------------------------------------------------------------------

-- povoli prekladacum stackovani predmety po N-kusech
local function apply_inserter_stacks(item)
    if inserter_max_stack_size < 0 then
        return
    end
    if item["max_belt_stack_size"] ~= nil and item.max_belt_stack_size > 1 then
        if item.max_belt_stack_size < inserter_max_stack_size then
            item.max_belt_stack_size = inserter_max_stack_size
        end
    end
end

-- prida extra modul sloty do kazde budovy
local function apply_module_slots(item)
    if item["module_slots"] ~= nil then
        local slots = math.abs(item.module_slots * module_slots_mult)
        item.module_slots = math.min(slots, 20);
    end
end

-- vylepsi beacon / majak
local function apply_beacon_boost(item)
    if beacon_boost_mult == 0 then
        return
    end
    if item["distribution_effectivity_bonus_per_quality_level"] ~= nil then
        item.distribution_effectivity = 1.5 * beacon_boost_mult
        item.distribution_effectivity_bonus_per_quality_level = 1.2 * beacon_boost_mult
        item.quality_indicator_scale = 1.0 * beacon_boost_mult
        item.supply_area_distance = math.floor(4 * beacon_area_mult)
    end
end

-- odstrani omezeni pro moduly
local function apply_no_modules_restrictions(item)
    if no_modules_restrictions then
        if item.name ~= "burner-mining-drill" and item.name ~= "stone-furnace" then
            if (item["allowed_effects"] ~= nil) then
                item.allowed_effects = { "consumption", "speed", "productivity", "pollution", "quality" }
            end
            -- povoli, aby vsechny stroje mohli prijimat efektivitu modulu z majaku
            if (item["effect_receiver"] ~= nil) then
                item.effect_receiver.uses_beacon_effects = true
            end
        end
    end
end

-- upravi minimalni velikost stacku predmetu
local function apply_min_stack_size(item)
    if item["stack_size"] ~= nil and item["stack_size"] > 1 then
        item.stack_size = math.max(min_stack_size, item.stack_size)
    end
end

-- upravi prutok v trubkach
local function apply_pipe_volume(item)
    if ic.has_value(fluid_volume_entities, item.name) or next(fluid_volume_entities) == nil then
        if item["fluid_box"] ~= nil and item["fluid_box"]["volume"] ~= nil then
            item.fluid_box.volume = item.fluid_box.volume * fluid_volume_mult
        end
    end
end

-- umozni stavet budovy na vsech planetach
local function apply_construction_restrictions(item)
    if construction_restrictions then
        if (item["surface_conditions"] ~= nil) then
            item.surface_conditions = nil
        end
    end
end

-- projde vsechny kategorie a objekty ve hre

for _, obj in pairs(data.raw) do
    for _, item in pairs(obj) do
        apply_inserter_stacks(item)
        apply_module_slots(item)
        apply_beacon_boost(item)
        apply_no_modules_restrictions(item)
        apply_min_stack_size(item)
        apply_pipe_volume(item)
        apply_construction_restrictions(item)
    end
end

----------------------------------------------------------------------------------------------------------------------------------
-- odebere 300% cap ze vsech receptu
--

local remove_productivity_recipe_cap = settings.startup["ic-tweaks-remove-productivity-recipe-cap"].value

if remove_productivity_recipe_cap then
    for _, r in pairs(data.raw.recipe) do
        r.maximum_productivity = 99999.99 -- vychozi: 3.0 (300%)
        r.allow_productivity = true
        r.allow_quality = true
        r.allow_speed = true
    end
end

----------------------------------------------------------------------------------------------------------------------------------
-- odebere penalty modulum a upravi je
--

local remove_module_speed_penalty = settings.startup["ic-tweaks-remove-module-speed-penalty"].value
local remove_module_quality_penalty = settings.startup["ic-tweaks-remove-module-quality-penalty"].value
local module_speed_mult = settings.startup["ic-tweaks-module-speed-mult"].value
local module_consumption_mult = settings.startup["ic-tweaks-module-consumption-mult"].value
local module_pollution_mult = settings.startup["ic-tweaks-module-pollution-mult"].value
local module_productivity_mult = settings.startup["ic-tweaks-module-productivity-mult"].value
local module_quality_mult = settings.startup["ic-tweaks-module-quality-mult"].value

for _, mod in pairs(data.raw.module) do
    local effects = mod.effect

    -- odebere penaltu rychlosti ze vsech modulu
    if remove_module_speed_penalty then
        if effects.speed ~= nil and effects.speed < 0 then
            effects.speed = 0
        end
    end
    -- Odebere penaltu kvality ze vsech modulu
    if remove_module_quality_penalty then
        if effects.quality ~= nil and effects.quality < 0 then
            effects.quality = 0
        end
    end
    if effects.speed ~= nil and effects.speed > 0 then
        effects.speed = effects.speed * module_speed_mult
    end
    if effects.consumption ~= nil and effects.consumption > 0 then
        effects.consumption = effects.consumption * module_consumption_mult
    end
    if effects.pollution ~= nil and effects.pollution > 0 then
        effects.pollution = effects.pollution * module_pollution_mult
    end
    if effects.productivity ~= nil and effects.productivity > 0 then
        effects.productivity = effects.productivity * module_productivity_mult
    end
    if effects.quality ~= nil and effects.quality > 0 then
        effects.quality = effects.quality * module_quality_mult
    end
    mod.effect = effects
end

----------------------------------------------------------------------------------------------------------------------------------
-- prida barvu pro majaky, ktery pouzivaji moduly produktivity a kvality
--

local colored_beacons_on_modules = settings.startup["ic-tweaks-colored-beacons-on-modules"].value

if colored_beacons_on_modules then
    for _, prod_module in pairs({ "productivity-module", "productivity-module-2", "productivity-module-3" }) do
        data.raw.module[prod_module].art_style = "vanilla"
        data.raw.module[prod_module].requires_beacon_alt_mode = false
        data.raw.module[prod_module].beacon_tint =
        {
            primary = { 1, 0.278, 0.082, 1 },
            secondary = { 1, 0.996, 0.184, 1 },
        }
    end

    for _, qual_module in pairs({ "quality-module", "quality-module-2", "quality-module-3" }) do
        data.raw.module[qual_module].art_style = "vanilla"
        data.raw.module[qual_module].requires_beacon_alt_mode = false
        data.raw.module[qual_module].beacon_tint =
        {
            primary = { 1, 1, 1, 1 },
            secondary = { 1, 0.149, 0.149, 1 },
        }
    end
end

----------------------------------------------------------------------------------------------------------------------------------
-- umozni ve vesmiru stavet truhly/bedny
--

local allow_chests_in_space = settings.startup["ic-tweaks-allow-chests-in-space"].value

if allow_chests_in_space then
    for _, type in pairs({ "container", "logistic-container" }) do
        for _, c in pairs(data.raw[type]) do
            c.surface_conditions = {
                {
                    property = "gravity",
                    min = nil
                }
            }
        end
    end
end

----------------------------------------------------------------------------------------------------------------------------------
-- velikost inventare
--

local inventory_size = settings.startup["ic-tweaks-inventory-size"].value
if inventory_size > 0 then
    data.raw.character.character.inventory_size = inventory_size
end

----------------------------------------------------------------------------------------------------------------------------------
-- rychlost vsech pasu apod.
--

local belt_speed_multiplier = settings.startup['ic-tweaks-belt-speed-multiplier'].value
local transport_belt_types = {
    "transport-belt",
    "underground-belt",
    "splitter",
    "loader",
    "loader-1x1",
    "linked-belt",
    "lane-splitter"
}
for _, cat in pairs(transport_belt_types) do
    local type = data.raw[cat]

    for _, e in pairs(type) do
        e.speed = e.speed * belt_speed_multiplier
    end
end
