-- povoli automaticke vylepseni truhel od drevene, zelezne, ocelove po pasivne poskytujici (cervenou)
if settings.startup["ic-better-logistic-containers-allow-upgrades"].value then
    local wooden_chest = data.raw["container"]["wooden-chest"]
    local iron_chest = data.raw["container"]["iron-chest"]
    local steel_chest = data.raw["container"]["steel-chest"]
    local passive_provider_chest = data.raw["logistic-container"]["passive-provider-chest"]

    wooden_chest.fast_replaceable_group = "container"
    wooden_chest.next_upgrade = iron_chest.name

    iron_chest.fast_replaceable_group = "container"
    iron_chest.next_upgrade = steel_chest.name

    steel_chest.fast_replaceable_group = "container"
    steel_chest.next_upgrade = passive_provider_chest.name
end

-- logisticky system bez nutnosti vesmirneho balicku
if settings.startup["ic-better-logistic-containers-logistic-system-nowhite"].value then
    local space_platform = data.raw.technology["space-platform-thruster"]

    -- upravi pouze pro DLC
    if space_platform then
        local system = data.raw.technology["logistic-system"]
        system.prerequisites = { "construction-robotics", "logistic-robotics", "utility-science-pack" }
        system.unit =
        {
            count = 500,
            ingredients =
            {
                { "automation-science-pack", 1 },
                { "logistic-science-pack",   1 },
                { "chemical-science-pack",   1 },
                { "utility-science-pack",    1 }
            },
            time = 30
        }
    end
end