data:extend(
    {
        {
            type = "int-setting",
            name = "ic-better-logistic-containers-transmission-delay",
            order = "transmission-a",
            setting_type = "startup",
            default_value = 60,
            minimum_value = 1,
            maximum_value = 216000
        },
        {
            type = "bool-setting",
            name = "ic-better-logistic-containers-transmission-isolated",
            order = "transmission-b",
            setting_type = "startup",
            default_value = false
        },
        {
            type = "bool-setting",
            name = "ic-better-logistic-containers-quality-booster",
            order = "transmission-c",
            setting_type = "startup",
            default_value = false
        },
        {
            type = "bool-setting",
            name = "ic-better-logistic-containers-allow-upgrades",
            order = "x-a",
            setting_type = "startup",
            default_value = true
        },
        {
            type = "bool-setting",
            name = "ic-better-logistic-containers-logistic-system-nowhite",
            order = "x-b",
            setting_type = "startup",
            default_value = true
        }
    }
)
