require("util")

if script.active_mods["gvv"] then require("__gvv__.gvv")() end

local function is_active_mod_quality()
  return script.active_mods["quality"]
end

local function is_active_mod_space_age()
  return script.active_mods["space-age"]
end

local conf_transmission_delay = settings.startup["ic-better-logistic-containers-transmission-delay"].value
local conf_transmission_isolated = settings.startup["ic-better-logistic-containers-transmission-isolated"].value
local conf_quality_booster = settings.startup["ic-better-logistic-containers-quality-booster"].value

local entity_passive_provider_chest = "passive-provider-chest"
local entity_requester_chest = "requester-chest"

-- seznam entit, pro ktere chceme odchytit udalosti
local entity_filter = {
  -- odesilaci (OUT)
  { filter = "name", name = entity_passive_provider_chest },
  -- prijimaci (IN)
  { filter = "name", name = entity_requester_chest }
}

-- pripravi vsechny dostupne kvality; potom se podle indexu ziska nazev (mensi dopad na vykon)
local available_qualities = {}
for _, quality in pairs(prototypes.quality) do
  if quality.name ~= "quality-unknown" then
    table.insert(available_qualities, quality.name)
  end
end

local function starts_with(str, start)
  return str:sub(1, #start) == start
end

local function ends_with(str, ending)
  return ending == "" or str:sub(- #ending) == ending
end

local function rpairs(tbl)
  return function(tbl, i)
    i = i - 1
    if i ~= 0 then
      return i, tbl[i]
    end
  end, tbl, #tbl + 1
end

-- zalozi truhlu do storage; nejenom pro nove vytvorene truhly
local function try_init_data()
  if not storage.logistic_chests then
    storage.logistic_chests = {}
    -- cervena bedna - odesilaci
    storage.logistic_chests[entity_passive_provider_chest] = {}
    -- modra bedna - prijimaci
    storage.logistic_chests[entity_requester_chest] = {}
  end
end

-- filtrace, aby logika probehla pouze pro zvolene entity
local function is_filtered_entity(entity)
  for _, data in pairs(entity_filter) do
    if entity.name == data.name then
      return true
    end
  end
  return false
end

-- ziska (nase) rozsirujici data na entite ve storage
local function get_entity_data(entity)
  if is_filtered_entity(entity) and storage.logistic_chests then
    local chests = storage.logistic_chests[entity.name]

    if chests then
      for id, data in pairs(chests) do
        if data.entity and data.entity.valid and entity.unit_number == id then
          return data
        end
      end
    end
  end
  return nil
end

-- ziska (nase) rozsirujici data na entite ve storage podle ID entity
local function get_entity_data_by_id(input_id)
  if not input_id then
    return nil
  end
  if storage.logistic_chests then
    for _, filter in pairs(entity_filter) do
      local chests = storage.logistic_chests[filter.name]

      if chests then
        for id, data in pairs(chests) do
          if data.entity and data.entity.valid and input_id == id then
            return data
          end
        end
      end
    end
  end
  return nil
end

-- unikatni retezec s ID signaly bedny; nevyplneny signal ma textovou hodnotu nil.nil (protoze jsou 2 signaly)
local function get_linkid_string(data)
  local str = ""

  if data.linkid_1.value then
    str = str .. data.linkid_1.value.name
    if data.linkid_1.value.quality and data.linkid_1.value.quality ~= "normal" then
      str = str .. "_" .. data.linkid_1.value.quality
    end
  else
    str = str .. "nil"
  end
  str = str .. "."

  if data.linkid_2.value then
    str = str .. data.linkid_2.value.name
    if data.linkid_2.value.quality and data.linkid_2.value.quality ~= "normal" then
      str = str .. "_" .. data.linkid_2.value.quality
    end
  else
    str = str .. "nil"
  end
  return str
end

-- unikatni retezec s ID povrchu/mista; nevyplneny signal ma textovou hodnotu nil
local function get_surfaceid_string(data)
  local str = ""

  if data.surfaceid and data.surfaceid.value then
    str = str .. data.surfaceid.value.name
    if data.surfaceid.value.quality and data.surfaceid.value.quality ~= "normal" then
      str = str .. "_" .. data.surfaceid.value.quality
    end
  else
    str = str .. "nil"
  end
  return str
end

local function has_selected_linkid(linkid_value)
  return linkid_value ~= "nil.nil"
end

local function has_selected_surfaceid(surfaceid_value)
  return surfaceid_value ~= "nil"
end

-- generuje seznam kvalit do drop-down
local function generate_quality_items(player)
  local list = {}
  for _, quality in pairs(prototypes.quality) do
    if quality.name ~= "quality-unknown" and player.force.is_quality_unlocked(quality.name) then
      table.insert(list, { "", "[quality=", quality.name, "] ", quality.localised_name })
    end
  end
  return list
end

-- ziska nahodnou hodnotu v tabule (na vstupu je napr. seznam vystupnich beden)
local function get_random(table)
  local choice = nil
  local n = 0

  for _, o in pairs(table) do
    n = n + 1
    if math.random() < (1 / n) then
      choice = o
    end
  end
  return choice
end

-- jak nazev rika, tak presune predmet do vystupni bedny a pripadne ho upravi
local function try_move_to_inventory(sender_data, inventory, new_inventory, orig_item)
  if new_inventory.is_full() then
    return
  end
  local item = orig_item
  -- pokud je moznost menit kvalitu
  if is_active_mod_quality() and sender_data.quality_booster and sender_data.quality_booster.value then
    local new_quality_name = available_qualities[sender_data.quality_booster.value]
    if new_quality_name and new_quality_name ~= item.quality then
      item = table.deepcopy(orig_item) -- nutne udelat kopii, potoze jinak neni mozne smazat predmet z truhly
      item.quality = new_quality_name
    end
  end
  -- posle predmet do prijimaci truhly
  local inserted = new_inventory.insert(item)
  if item.count == inserted then
    -- vsechny itemy byly vlozeny
    inventory.remove(orig_item)
  elseif inserted < item.count then
    -- vlozena pouze cast predmetu, protoze inventar se zaplnil
    local new_count = item.count - inserted
    inventory.remove(orig_item)

    if is_active_mod_quality() then
      inventory.insert({ name = item.name, count = new_count, quality = item.quality })
    else
      inventory.insert({ name = item.name, count = new_count })
    end
  end
end

-- preddefinovane ikonky povrchu pro umisteni truhly
local function resolve_default_surfaceid(entity)
  local name = entity.surface.name
  if name == "nauvis" then
    return { type = "space-location", name = "nauvis" }
  end
  if name == "vulcanus" then
    return { type = "space-location", name = "vulcanus" }
  end
  if name == "gleba" then
    return { type = "space-location", name = "gleba" }
  end
  if name == "fulgora" then
    return { type = "space-location", name = "fulgora" }
  end
  if name == "aquilo" then
    return { type = "space-location", name = "aquilo" }
  end
  -- podpora staveni truhel ve vesmiru
  if starts_with(name, "platform-") then
    return { type = "space-location", name = "solar-system-edge" }
  end
  -- podpora pro factorissimo3
  if name:match("factory") then
    return { name = "assembling-machine-3" }
  end
  return nil
end

-- vyhleda pro vstupni truhlu vsechny vystupni a vybere z nich jednu nahodnou
local function find_input_data(output_linkid_name, output_surfaceid_name)
  local entities = {}
  local result = nil

  -- projde vsechny prijimaci bedny (muze jich byt vice)
  for _, in_data in pairs(storage.logistic_chests[entity_requester_chest]) do
    -- souhlasi a existuje cilove ID bedny
    local in_linkid_str = get_linkid_string(in_data)
    if in_linkid_str == output_linkid_name and has_selected_linkid(in_linkid_str) then
      -- souhlasi ID ciloveho povrchu bedny
      local in_surfaceid_str = get_surfaceid_string(in_data)
      if in_surfaceid_str == output_surfaceid_name then
        table.insert(entities, in_data)
        result = in_data
      end
    end
  end
  -- pokud bylo nalezeno vice cilovych beden, tak vlozi predmet do nahodne vystupni bedny
  if #entities > 1 then
    result = get_random(entities)
  end
  return result
end

-- EVENT, postaveni truhly
local function on_built_entity(entity)
  try_init_data()

  if is_filtered_entity(entity) then
    -- priprava extra informaci pro bedny, ktere se drzi v save hry
    storage.logistic_chests[entity.name][entity.unit_number] = {
      id = entity.unit_number,
      entity = entity,
      surfaceid = {
        element = nil,
        value = resolve_default_surfaceid(entity)
      },
      linkid_1 = {
        element = nil,
        value = nil
      },
      linkid_2 = {
        element = nil,
        value = nil
      },
      linkid_autoselect = {
        element = nil
      },
      quality_booster = {
        element = nil,
        value = 1
      }
    }
  end
end

-- EVENT, postaveni truhly
local function on_built(evt)
  on_built_entity(evt.entity)
end

-- EVENT, zniceni truhly
local function on_destroy(evt)
  local data = get_entity_data(evt.entity)
  if data then
    -- entita byla jakkoliv znicena
    storage.logistic_chests[evt.entity.name][evt.entity.unit_number] = nil
  end
end

-- EVENT, otevreni nebo zavreni GUI truhly
local function on_gui(evt, opened)
  local player_index = evt.player_index
  local player = game.get_player(player_index)
  local entity_gui_name = evt.entity.name .. "-gui"
  local data = get_entity_data(evt.entity)
  local sender = evt.entity.name == entity_passive_provider_chest

  -- dealokace
  if player.gui and player.gui.valid then
    local gui = player.gui.relative[entity_gui_name]
    if gui and gui.valid then
      gui.destroy()
    end
  end

  -- pri zavreni okna se vse dalsi ignoruje
  if not opened then
    return
  end

  -- prida entitu, ktera byla postavena jeste bez modu pro zpetnou kompatibilitu
  if not data then
    on_built(evt)
    data = get_entity_data(evt.entity)
  end

  local gui_frame_caption = nil
  if sender then
    gui_frame_caption = { "gui.transmission-src" }
  else
    gui_frame_caption = { "gui.transmission-dst" }
  end

  local gui_signal_tooltip = nil
  if sender then
    gui_signal_tooltip = { "tooltip.transmission-src" }
  else
    gui_signal_tooltip = { "tooltip.transmission-dst" }
  end

  -- hlavni okno modu, ktere bude umisteno po prave strane hlavniho okna truhly
  local gui = player.gui.relative.add({
    type = "frame",
    name = entity_gui_name,
    caption = gui_frame_caption,
    direction = "vertical",
    index = 0,
    anchor = {
      gui = defines.relative_gui_type.container_gui,
      position = defines.relative_gui_position.right
    }
  })

  local gui_inside = gui.add({
    type = "frame",
    style = "inside_shallow_frame_with_padding",
    direction = "vertical"
  })

  -- radek s povrchem/mistem
  if data.surfaceid then
    local surface_flow = gui_inside.add({
      type = "flow",
      style = "player_input_horizontal_flow"
    })
    surface_flow.add({
      type = "label",
      caption = { "gui.surfaceid" },
      tooltip = { "tooltip.surfaceid" },
    })
    data.surfaceid.element = surface_flow.add({
      type = "choose-elem-button",
      name = entity_gui_name .. "-inside-flow_surfaceid-button",
      style = "slot_button_in_shallow_frame",
      elem_type = "signal",
      signal = data.surfaceid.value,
      enabled = not conf_transmission_isolated
    })
    -- mezera je zde, protoze zpetna kompatibilita se starsi verzi modu
    local surfaceid_line_gap_flow = gui_inside.add({
      type = "flow",
      style = "player_input_horizontal_flow"
    })
    surfaceid_line_gap_flow.add({
      type = "line"
    })
  end

  -- radek se signaly
  local signal_flow = gui_inside.add({
    type = "flow",
    style = "player_input_horizontal_flow"
  })
  signal_flow.add({
    type = "label",
    caption = { "gui.linkid" },
    tooltip = gui_signal_tooltip
  })
  data.linkid_1.element = signal_flow.add({
    type = "choose-elem-button",
    name = entity_gui_name .. "-inside-flow_linkid-button-1",
    style = "slot_button_in_shallow_frame",
    elem_type = "signal",
    signal = data.linkid_1.value
  })
  data.linkid_2.element = signal_flow.add({
    type = "choose-elem-button",
    name = entity_gui_name .. "-inside-flow_linkid-button-2",
    style = "slot_button_in_shallow_frame",
    elem_type = "signal",
    signal = data.linkid_2.value
  })

  -- radek se zmenou kvality
  if data.quality_booster and sender then
    local quality_line_gap_flow = gui_inside.add({
      type = "flow",
      style = "player_input_horizontal_flow"
    })
    quality_line_gap_flow.add({
      type = "line"
    })
    local quality_flow = gui_inside.add({
      type = "flow",
      style = "player_input_horizontal_flow"
    })
    quality_flow.add({
      type = "label",
      caption = { "gui.quality-booster" },
      tooltip = { "tooltip.quality-booster" }
    })
    data.quality_booster.element = quality_flow.add({
      type = "drop-down",
      name = entity_gui_name .. "-inside-flow-qualitybooster-select",
      caption = { "gui.quality-booster" },
      items = generate_quality_items(player),
      selected_index = data.quality_booster.value,
      enabled = conf_quality_booster
    })
  end

  -- radek s tlacitky
  if data.linkid_autoselect then
    local line_flow = gui_inside.add({
      type = "flow",
      style = "player_input_horizontal_flow"
    })
    line_flow.add({
      type = "line"
    })
    local button_flow = gui_inside.add({
      type = "flow",
      style = "dialog_buttons_horizontal_flow"
    })
    data.linkid_autoselect.element = button_flow.add({
      type = "button",
      name = entity_gui_name .. "-inside-flow_linkid-autoselect",
      caption = { "gui.auto-select" },
      mouse_button_filter = { "left" },
    })
  end
end

-- EVENT, otevreni GUI truhly
local function on_gui_opened(evt)
  if not evt.entity or not is_filtered_entity(evt.entity) then
    return
  end
  on_gui(evt, true)
end

-- EVENT, zavreni GUI truhly
local function on_gui_closed(evt)
  if not evt.entity or not is_filtered_entity(evt.entity) then
    return
  end
  on_gui(evt, false)
end

-- EVENT, zmena elementu (napr. signalu)
local function on_gui_changed(evt)
  if not evt.element then
    return
  end

  local player = game.get_player(evt.player_index)
  if not player or not player.opened then
    return
  end

  local opened_entity_id = player.opened.unit_number
  local data = get_entity_data_by_id(opened_entity_id)

  if data then
    if data.linkid_1.element.name == evt.element.name then
      data.linkid_1.value = evt.element.elem_value
    elseif data.linkid_2.element.name == evt.element.name then
      data.linkid_2.value = evt.element.elem_value
    elseif data.surfaceid and data.surfaceid.element.name == evt.element.name and not conf_transmission_isolated then
      data.surfaceid.value = evt.element.elem_value
    end
  end
end

-- EVENT, zmena v drop-down (napr. kvality)
local function on_gui_selected_state_changed(evt)
  if not evt.element then
    return
  end

  local player = game.get_player(evt.player_index)
  if not player or not player.opened then
    return
  end

  local opened_entity_id = player.opened.unit_number
  local data = get_entity_data_by_id(opened_entity_id)

  if data then
    if data.quality_booster and data.quality_booster.element.name == evt.element.name and conf_quality_booster then
      data.quality_booster.value = evt.element.selected_index or 1 -- indexuje se od 1-N
    end
  end
end

-- EVENT, kliknuti na tlacitka (napr. detekce)
local function on_gui_click(evt)
  if not evt.element or evt.element.type ~= "button" then
    return
  end

  local player = game.get_player(evt.player_index)
  if not player or not player.opened then
    return
  end

  local opened_entity_id = player.opened.unit_number
  local data = get_entity_data_by_id(opened_entity_id)

  if data then
    -- kliknulo se na tlacitko auto-detekce
    if evt.element.name == data.linkid_autoselect.element.name then
      -- pro izolovene povrchy nastavime vychozi hodnotu
      if data.surfaceid and not conf_transmission_isolated then
        local surfaceid = resolve_default_surfaceid(data.entity)
        if surfaceid then
          data.surfaceid.value = surfaceid
          data.surfaceid.element.elem_value = data.surfaceid.value
        end
      end

      local inv = data.entity.get_inventory(defines.inventory.chest)
      local contents = inv.get_contents()
      local max_item = nil -- item, ktereho je nejvice

      -- vyhleda predmet, ktereho je nejvice
      for _, item in pairs(contents) do
        if not max_item or item.count > max_item.count then
          max_item = item
        end
      end

      if max_item then
        if max_item.quality then
          data.linkid_1.value = { name = max_item.name, quality = max_item.quality }
        else
          data.linkid_1.value = { name = max_item.name }
        end
        data.linkid_1.element.elem_value = data.linkid_1.value
        data.linkid_2.value = nil
        data.linkid_2.element.elem_value = nil
      end
    end
  end
end

-- EVENT, kopirovani konfigurace entity pomoci shift
local function on_settings_pasted(evt)
  local source = get_entity_data(evt.source)
  local dest = get_entity_data(evt.destination)

  if source and dest then
    local dest_sender = dest.entity.name == entity_passive_provider_chest

    if source.surfaceid and not conf_transmission_isolated then
      dest.surfaceid = table.deepcopy(source.surfaceid)
    end
    if dest_sender and source.quality_booster and is_active_mod_quality() then
      dest.quality_booster = table.deepcopy(source.quality_booster)
    end
    dest.linkid_1 = table.deepcopy(source.linkid_1)
    dest.linkid_2 = table.deepcopy(source.linkid_2)
  end
end

-- logika tick EVENT, resi kopirovani polozek v truhlach
local function teleport_tick()
  if not storage.logistic_chests then
    return
  end
  -- projde vsechny vysilaci bedny
  for _, out_data in pairs(storage.logistic_chests[entity_passive_provider_chest]) do
    local out_linkid_str = get_linkid_string(out_data)

    -- na vysilaci bedne je vyplneno ID
    if has_selected_linkid(out_linkid_str) then
      local out_surfaceid_str = get_surfaceid_string(out_data)
      local in_data = find_input_data(out_linkid_str, out_surfaceid_str)

      -- pro ID existuje aspon jedna vystupni bedna
      if in_data then
        local out_inv = out_data.entity.get_inventory(defines.inventory.chest)
        local out_contents = out_inv.get_contents()
        local in_inv = in_data.entity.get_inventory(defines.inventory.chest)

        -- zkusi presunout predmety ze vstupni do vystupni bedny
        for _, content in pairs(out_contents) do
          try_move_to_inventory(out_data, out_inv, in_inv, content)
        end
      end
    end
  end
end

-- EVENT, pravidelneho ticku
local function on_tick()
  teleport_tick()
end

-- vytvoreni
script.on_event(defines.events.on_built_entity, on_built, entity_filter)
script.on_event(defines.events.on_robot_built_entity, on_built, entity_filter)
script.on_event(defines.events.script_raised_built, on_built, entity_filter)
script.on_event(defines.events.script_raised_revive, on_built, entity_filter)
script.on_event(defines.events.on_entity_cloned, function(evt) on_built_entity(evt.destination) end)

if is_active_mod_space_age() then
  script.on_event(defines.events.on_space_platform_built_entity, on_built, entity_filter)
end

-- smazani
script.on_event(defines.events.on_pre_player_mined_item, on_destroy)
script.on_event(defines.events.on_robot_pre_mined, on_destroy)
script.on_event(defines.events.on_robot_mined_entity, on_destroy)
script.on_event(defines.events.on_entity_died, on_destroy)
script.on_event(defines.events.script_raised_destroy, on_destroy)

if is_active_mod_space_age() then
  script.on_event(defines.events.on_space_platform_mined_entity, on_destroy)
end

-- otevreni, zavreni, zmena gui
script.on_event(defines.events.on_gui_opened, on_gui_opened)
script.on_event(defines.events.on_gui_closed, on_gui_closed)
script.on_event(defines.events.on_gui_elem_changed, on_gui_changed)
script.on_event(defines.events.on_gui_click, on_gui_click)
script.on_event(defines.events.on_gui_selection_state_changed, on_gui_selected_state_changed)

-- kopirovani konfigurace pres shift
script.on_event(defines.events.on_entity_settings_pasted, on_settings_pasted)

-- ostatni
script.on_nth_tick(conf_transmission_delay, on_tick)
