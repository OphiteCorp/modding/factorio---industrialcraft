local tech_mult = settings.startup["ic-reinforced-walls-technology-multiplier"].value

local function create_wall(mk_level, in_prerequisites, in_ingredients)
  return {
    type = "technology",
    name = "ic-reinforced-wall-mk" .. mk_level,
    icon = "__ic-reinforced-walls__/graphics/technology/ic-reinforced-wall-mk" .. mk_level .. ".png",
    icon_size = 256,
    effects = {
      {
        type = "unlock-recipe",
        recipe = "ic-reinforced-wall-mk" .. mk_level
      }
    },
    prerequisites = in_prerequisites,
    unit = {
      count = math.floor((100 * mk_level) * tech_mult),
      ingredients = in_ingredients,
      time = 10
    }
  }
end

local function create_gate(mk_level, in_prerequisites, in_ingredients)
  return {
    type = "technology",
    name = "ic-reinforced-gate-mk" .. mk_level,
    localised_description = { "technology-description.gates" },
    icon = "__ic-reinforced-walls__/graphics/technology/ic-reinforced-gate-mk" .. mk_level .. ".png",
    icon_size = 256,
    effects = {
      {
        type = "unlock-recipe",
        recipe = "ic-reinforced-gate-mk" .. mk_level
      }
    },
    prerequisites = in_prerequisites,
    unit = {
      count = math.floor((200 * mk_level) * tech_mult),
      ingredients = in_ingredients,
      time = 30
    }
  }
end

data:extend(
  {
    create_wall(
      1,
      {
        "stone-wall",
        "steel-processing"
      },
      {
        { "automation-science-pack", 2 },
        { "logistic-science-pack",   1 }
      }
    ),
    create_wall(
      2,
      {
        "ic-reinforced-wall-mk1",
        "low-density-structure"
      },
      {
        { "automation-science-pack", 3 },
        { "logistic-science-pack",   2 },
        { "chemical-science-pack",   1 }
      }
    ),
    create_wall(
      3,
      {
        "ic-reinforced-wall-mk2",
        "uranium-processing"
      },
      {
        { "automation-science-pack", 4 },
        { "logistic-science-pack",   3 },
        { "chemical-science-pack",   2 },
        { "utility-science-pack",    1 }
      }
    ),
    create_wall(
      4,
      {
        "ic-reinforced-wall-mk3",
        "rocket-silo"
      },
      {
        { "automation-science-pack", 5 },
        { "logistic-science-pack",   4 },
        { "chemical-science-pack",   3 },
        { "utility-science-pack",    2 },
        { "production-science-pack", 1 }
      }
    ),

    create_gate(
      1,
      {
        "ic-reinforced-wall-mk1",
        "gate"
      },
      {
        { "automation-science-pack", 2 },
        { "logistic-science-pack",   1 }
      }
    ),
    create_gate(
      2,
      {
        "ic-reinforced-gate-mk1",
        "ic-reinforced-wall-mk2"
      },
      {
        { "automation-science-pack", 3 },
        { "logistic-science-pack",   2 },
        { "chemical-science-pack",   1 }
      }
    ),
    create_gate(
      3,
      {
        "ic-reinforced-gate-mk2",
        "ic-reinforced-wall-mk3"
      },
      {
        { "automation-science-pack", 4 },
        { "logistic-science-pack",   3 },
        { "chemical-science-pack",   2 },
        { "utility-science-pack",    1 }
      }
    ),
    create_gate(
      4,
      {
        "ic-reinforced-gate-mk3",
        "ic-reinforced-wall-mk4"
      },
      {
        { "automation-science-pack", 5 },
        { "logistic-science-pack",   4 },
        { "chemical-science-pack",   3 },
        { "utility-science-pack",    2 },
        { "production-science-pack", 1 }
      }
    )
  }
)
