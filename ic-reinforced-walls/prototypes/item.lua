local item_sounds = require("__base__.prototypes.item_sounds")

local stack_size = 50

local function create_wall(mk_level)
  return {
    type = "item",
    name = "ic-reinforced-wall-mk" .. mk_level,
    icon = "__ic-reinforced-walls__/graphics/icons/ic-reinforced-wall-mk" .. mk_level .. ".png",
    icon_size = 64,
    icon_mipmaps = 4,
    subgroup = "defensive-structure",
    order = "a[wall]-a[wall]-[ic-wall-mk" .. mk_level .. "]",
    place_result = "ic-reinforced-wall-mk" .. mk_level,
    stack_size = stack_size,
    localised_name = { "item-name.ic-reinforced-wall-mk" .. mk_level },
    localised_description = { "item-description.ic-reinforced-wall-mk" .. mk_level }
  }
end

local function create_gate(mk_level)
  return {
    type = "item",
    name = "ic-reinforced-gate-mk" .. mk_level,
    icon = "__ic-reinforced-walls__/graphics/icons/ic-reinforced-gate-mk" .. mk_level .. ".png",
    subgroup = "defensive-structure",
    order = "a[wall]-b[gate]-[ic-gate-mk" .. mk_level .. "]",
    inventory_move_sound = item_sounds.concrete_inventory_move,
    pick_sound = item_sounds.concrete_inventory_pickup,
    drop_sound = item_sounds.concrete_inventory_move,
    place_result = "ic-reinforced-gate-mk" .. mk_level,
    stack_size = stack_size,
    localised_name = { "item-name.ic-reinforced-gate-mk" .. mk_level },
    localised_description = { "item-description.ic-reinforced-gate-mk" .. mk_level }
  }
end

data:extend(
  {
    create_wall(1),
    create_wall(2),
    create_wall(3),
    create_wall(4),

    create_gate(1),
    create_gate(2),
    create_gate(3),
    create_gate(4)
  }
)
