local util = require("util")
local hit_effects = require("__base__.prototypes.entity.hit-effects")
local sounds = require("__base__.prototypes.entity.sounds")
local simulations = require("__base__.prototypes.factoriopedia-simulations")

local reflect_modif = settings.startup["ic-reinforced-walls-reflect-multiplier"].value

local function apply_reaction(entity, mk_level)
  -- pouze pro Tier 4
  if mk_level == 4 then
    local dmg_modif = reflect_modif

    entity.healing_per_tick = 0.1
    entity.attack_reaction = {
      {
        range = 1000,
        reaction_modifier = 0.25,
        action = {
          {
            type = "direct",
            force = "enemy",
            filter_enabled = true,
            action_delivery =
            {
              type = "instant",
              target_effects =
              {
                type = "nested-result",
                action =
                {
                  type = "direct",
                  action_delivery =
                  {
                    type = "instant",
                    target_effects =
                    {
                      type = "damage",
                      damage = { amount = math.floor(8 * dmg_modif), type = "poison" }
                    }
                  }
                },
                {
                  type = "direct",
                  action_delivery =
                  {
                    type = "instant",
                    target_effects =
                    {
                      type = "damage",
                      damage = { amount = math.floor(6 * dmg_modif), type = "electric" }
                    }
                  }
                },
                {
                  type = "direct",
                  action_delivery =
                  {
                    type = "instant",
                    target_effects =
                    {
                      type = "damage",
                      damage = { amount = math.floor(4 * dmg_modif), type = "fire" }
                    }
                  }
                },
                {
                  type = "direct",
                  action_delivery =
                  {
                    type = "instant",
                    target_effects =
                    {
                      type = "damage",
                      damage = { amount = math.floor(4 * dmg_modif), type = "laser" }
                    }
                  }
                },
                {
                  type = "direct",
                  action_delivery =
                  {
                    type = "instant",
                    target_effects =
                    {
                      type = "damage",
                      damage = { amount = math.floor(8 * dmg_modif), type = "acid" }
                    }
                  }
                },
                {
                  type = "direct",
                  action_delivery =
                  {
                    type = "instant",
                    target_effects =
                    {
                      type = "damage",
                      damage = { amount = math.floor(4 * dmg_modif), type = "explosion" }
                    }
                  }
                },
                {
                  type = "direct",
                  action_delivery =
                  {
                    type = "instant",
                    target_effects =
                    {
                      type = "damage",
                      damage = { amount = math.floor(4 * dmg_modif), type = "physical" }
                    }
                  }
                },
                {
                  type = "direct",
                  action_delivery =
                  {
                    type = "instant",
                    target_effects =
                    {
                      type = "damage",
                      damage = { amount = math.floor(4 * dmg_modif), type = "impact" }
                    }
                  }
                }
              }
            }
          },
        }
      }
    }
  end
end

local function create_wall(mk_level, health, in_resistances, in_next_upgrade)
  local e = {
    type = "wall",
    name = "ic-reinforced-wall-mk" .. mk_level,
    icon = "__ic-reinforced-walls__/graphics/icons/ic-reinforced-wall-mk" .. mk_level .. ".png",
    flags = { "placeable-neutral", "player-creation" },
    collision_box = { { -0.29, -0.29 }, { 0.29, 0.29 } },
    selection_box = { { -0.5, -0.5 }, { 0.5, 0.5 } },
    damaged_trigger_effect = hit_effects.wall(),
    minable = { mining_time = 0.2, result = "ic-reinforced-wall-mk" .. mk_level },
    fast_replaceable_group = "wall",
    max_health = health,
    repair_speed_modifier = 2,
    corpse = "wall-remnants",
    next_upgrade = in_next_upgrade,
    dying_explosion = "wall-explosion",
    repair_sound = sounds.manual_repair,
    mined_sound = sounds.deconstruct_bricks(0.8),
    open_sound = sounds.machine_open,
    close_sound = sounds.machine_close,
    impact_category = "stone",
    connected_gate_visualization = {
      filename = "__core__/graphics/arrows/underground-lines.png",
      priority = "high",
      width = 64,
      height = 64,
      scale = 0.5
    },
    resistances = in_resistances,
    visual_merge_group = 0,
    pictures = {
      single = {
        layers = {
          {
            filename = "__ic-reinforced-walls__/graphics/entity/wall-mk" .. mk_level .. "/wall-single.png",
            priority = "extra-high",
            width = 64,
            height = 86,
            variation_count = 2,
            line_length = 2,
            shift = util.by_pixel(0, -5),
            scale = 0.5
          },
          {
            filename = "__base__/graphics/entity/wall/wall-single-shadow.png",
            priority = "extra-high",
            width = 98,
            height = 60,
            repeat_count = 2,
            shift = util.by_pixel(10, 17),
            draw_as_shadow = true,
            scale = 0.5
          }
        }
      },
      straight_vertical = {
        layers = {
          {
            filename = "__ic-reinforced-walls__/graphics/entity/wall-mk" .. mk_level .. "/wall-vertical.png",
            priority = "extra-high",
            width = 64,
            height = 134,
            variation_count = 5,
            line_length = 5,
            shift = util.by_pixel(0, 8),
            scale = 0.5
          },
          {
            filename = "__base__/graphics/entity/wall/wall-vertical-shadow.png",
            priority = "extra-high",
            width = 98,
            height = 110,
            repeat_count = 5,
            shift = util.by_pixel(10, 29),
            draw_as_shadow = true,
            scale = 0.5
          }
        }
      },
      straight_horizontal = {
        layers = {
          {
            filename = "__ic-reinforced-walls__/graphics/entity/wall-mk" .. mk_level .. "/wall-horizontal.png",
            priority = "extra-high",
            width = 64,
            height = 92,
            variation_count = 6,
            line_length = 6,
            shift = util.by_pixel(0, -2),
            scale = 0.5
          },
          {
            filename = "__base__/graphics/entity/wall/wall-horizontal-shadow.png",
            priority = "extra-high",
            width = 124,
            height = 68,
            repeat_count = 6,
            shift = util.by_pixel(14, 15),
            draw_as_shadow = true,
            scale = 0.5
          }
        }
      },
      corner_right_down = {
        layers = {
          {
            filename = "__ic-reinforced-walls__/graphics/entity/wall-mk" .. mk_level .. "/wall-corner-right.png",
            priority = "extra-high",
            width = 64,
            height = 128,
            variation_count = 2,
            line_length = 2,
            shift = util.by_pixel(0, 7),
            scale = 0.5
          },
          {
            filename = "__base__/graphics/entity/wall/wall-corner-right-shadow.png",
            priority = "extra-high",
            width = 124,
            height = 120,
            repeat_count = 2,
            shift = util.by_pixel(17, 28),
            draw_as_shadow = true,
            scale = 0.5
          }
        }
      },
      corner_left_down = {
        layers = {
          {
            filename = "__ic-reinforced-walls__/graphics/entity/wall-mk" .. mk_level .. "/wall-corner-left.png",
            priority = "extra-high",
            width = 64,
            height = 134,
            variation_count = 2,
            line_length = 2,
            shift = util.by_pixel(0, 7),
            scale = 0.5
          },
          {
            filename = "__base__/graphics/entity/wall/wall-corner-left-shadow.png",
            priority = "extra-high",
            width = 102,
            height = 120,
            repeat_count = 2,
            shift = util.by_pixel(9, 28),
            draw_as_shadow = true,
            scale = 0.5
          }
        }
      },
      t_up = {
        layers = {
          {
            filename = "__ic-reinforced-walls__/graphics/entity/wall-mk" .. mk_level .. "/wall-t.png",
            priority = "extra-high",
            width = 64,
            height = 134,
            variation_count = 4,
            line_length = 4,
            shift = util.by_pixel(0, 7),
            scale = 0.5
          },
          {
            filename = "__base__/graphics/entity/wall/wall-t-shadow.png",
            priority = "extra-high",
            width = 124,
            height = 120,
            repeat_count = 4,
            shift = util.by_pixel(14, 28),
            draw_as_shadow = true,
            scale = 0.5
          }
        }
      },
      ending_right = {
        layers = {
          {
            filename = "__ic-reinforced-walls__/graphics/entity/wall-mk" .. mk_level .. "/wall-ending-right.png",
            priority = "extra-high",
            width = 64,
            height = 92,
            variation_count = 2,
            line_length = 2,
            shift = util.by_pixel(0, -3),
            scale = 0.5
          },
          {
            filename = "__base__/graphics/entity/wall/wall-ending-right-shadow.png",
            priority = "extra-high",
            width = 124,
            height = 68,
            repeat_count = 2,
            shift = util.by_pixel(17, 15),
            draw_as_shadow = true,
            scale = 0.5
          }
        }
      },
      ending_left = {
        layers = {
          {
            filename = "__ic-reinforced-walls__/graphics/entity/wall-mk" .. mk_level .. "/wall-ending-left.png",
            priority = "extra-high",
            width = 64,
            height = 92,
            variation_count = 2,
            line_length = 2,
            shift = util.by_pixel(0, -3),
            scale = 0.5
          },
          {
            filename = "__base__/graphics/entity/wall/wall-ending-left-shadow.png",
            priority = "extra-high",
            width = 102,
            height = 68,
            repeat_count = 2,
            shift = util.by_pixel(9, 15),
            draw_as_shadow = true,
            scale = 0.5
          }
        }
      },
      filling = {
        filename = "__ic-reinforced-walls__/graphics/entity/wall-mk" .. mk_level .. "/wall-filling.png",
        priority = "extra-high",
        width = 48,
        height = 56,
        variation_count = 8,
        line_length = 8,
        shift = util.by_pixel(0, -1),
        scale = 0.5
      },
      water_connection_patch = {
        sheets = {
          {
            filename = "__ic-reinforced-walls__/graphics/entity/wall-mk" .. mk_level .. "/wall-patch.png",
            priority = "extra-high",
            width = 116,
            height = 128,
            shift = util.by_pixel(0, -2),
            scale = 0.5
          },
          {
            filename = "__base__/graphics/entity/wall/wall-patch-shadow.png",
            priority = "extra-high",
            width = 144,
            height = 100,
            shift = util.by_pixel(9, 15),
            draw_as_shadow = true,
            scale = 0.5
          }
        }
      },
      gate_connection_patch = {
        sheets = {
          {
            filename = "__ic-reinforced-walls__/graphics/entity/wall-mk" .. mk_level .. "/wall-gate.png",
            priority = "extra-high",
            width = 82,
            height = 108,
            shift = util.by_pixel(0, -7),
            scale = 0.5
          },
          {
            filename = "__base__/graphics/entity/wall/wall-gate-shadow.png",
            priority = "extra-high",
            width = 130,
            height = 78,
            shift = util.by_pixel(14, 18),
            draw_as_shadow = true,
            scale = 0.5
          }
        }
      }
    },
    wall_diode_green = {
      sheet = {
        filename = "__base__/graphics/entity/wall/wall-diode-green.png",
        priority = "extra-high",
        width = 72,
        height = 44,
        draw_as_glow = true,
        --frames = 4,
        shift = util.by_pixel(-1, -23),
        scale = 0.5
      }
    },
    wall_diode_green_light_top = {
      minimum_darkness = 0.3,
      color = { g = 1 },
      shift = util.by_pixel(0, -30),
      size = 1,
      intensity = 0.2
    },
    wall_diode_green_light_right = {
      minimum_darkness = 0.3,
      color = { g = 1 },
      shift = util.by_pixel(12, -23),
      size = 1,
      intensity = 0.2
    },
    wall_diode_green_light_bottom = {
      minimum_darkness = 0.3,
      color = { g = 1 },
      shift = util.by_pixel(0, -17),
      size = 1,
      intensity = 0.2
    },
    wall_diode_green_light_left = {
      minimum_darkness = 0.3,
      color = { g = 1 },
      shift = util.by_pixel(-12, -23),
      size = 1,
      intensity = 0.2
    },
    wall_diode_red = {
      sheet = {
        filename = "__base__/graphics/entity/wall/wall-diode-red.png",
        priority = "extra-high",
        width = 72,
        height = 44,
        draw_as_glow = true,
        --frames = 4,
        shift = util.by_pixel(-1, -23),
        scale = 0.5
      }
    },
    wall_diode_red_light_top = {
      minimum_darkness = 0.3,
      color = { r = 1 },
      shift = util.by_pixel(0, -30),
      size = 1,
      intensity = 0.2
    },
    wall_diode_red_light_right = {
      minimum_darkness = 0.3,
      color = { r = 1 },
      shift = util.by_pixel(12, -23),
      size = 1,
      intensity = 0.2
    },
    wall_diode_red_light_bottom = {
      minimum_darkness = 0.3,
      color = { r = 1 },
      shift = util.by_pixel(0, -17),
      size = 1,
      intensity = 0.2
    },
    wall_diode_red_light_left = {
      minimum_darkness = 0.3,
      color = { r = 1 },
      shift = util.by_pixel(-12, -23),
      size = 1,
      intensity = 0.2
    },
    circuit_connector = circuit_connector_definitions["wall"],
    circuit_wire_max_distance = default_circuit_wire_max_distance,
    default_output_signal = { type = "virtual", name = "signal-G" }
  }
  apply_reaction(e, mk_level)
  return e
end

local function create_gate(mk_level, health, in_resistances, in_next_upgrade)
  local e = {
    type = "gate",
    name = "ic-reinforced-gate-mk" .. mk_level,
    icon = "__ic-reinforced-walls__/graphics/icons/ic-reinforced-gate-mk" .. mk_level .. ".png",
    flags = { "placeable-neutral", "placeable-player", "player-creation" },
    fast_replaceable_group = "wall",
    minable = { mining_time = 0.1, result = "ic-reinforced-gate-mk" .. mk_level },
    max_health = health,
    corpse = "gate-remnants",
    next_upgrade = in_next_upgrade,
    dying_explosion = "gate-explosion",
    factoriopedia_simulation = simulations.factoriopedia_gate,
    collision_box = { { -0.29, -0.29 }, { 0.29, 0.29 } },
    selection_box = { { -0.5, -0.5 }, { 0.5, 0.5 } },
    damaged_trigger_effect = hit_effects.entity(),
    opening_speed = 0.085,
    activation_distance = 3,
    timeout_to_close = 5,
    fadeout_interval = 15,
    resistances = in_resistances,
    vertical_animation =
    {
      layers =
      {
        {
          filename = "__base__/graphics/entity/gate/gate-vertical.png",
          line_length = 8,
          width = 78,
          height = 120,
          frame_count = 16,
          shift = util.by_pixel(-1, -13),
          scale = 0.5
        },
        {
          filename = "__base__/graphics/entity/gate/gate-vertical-shadow.png",
          line_length = 8,
          width = 82,
          height = 104,
          frame_count = 16,
          shift = util.by_pixel(9, 9),
          draw_as_shadow = true,
          scale = 0.5
        }
      }
    },
    horizontal_animation =
    {
      layers =
      {
        {
          filename = "__base__/graphics/entity/gate/gate-horizontal.png",
          line_length = 8,
          width = 66,
          height = 90,
          frame_count = 16,
          shift = util.by_pixel(0, -3),
          scale = 0.5
        },
        {
          filename = "__base__/graphics/entity/gate/gate-horizontal-shadow.png",
          line_length = 8,
          width = 122,
          height = 60,
          frame_count = 16,
          shift = util.by_pixel(12, 10),
          draw_as_shadow = true,
          scale = 0.5
        }
      }
    },
    horizontal_rail_animation_left =
    {
      layers =
      {
        {
          filename = "__base__/graphics/entity/gate/gate-rail-horizontal-left.png",
          line_length = 8,
          width = 66,
          height = 74,
          frame_count = 16,
          shift = util.by_pixel(0, -7),
          scale = 0.5
        },
        {
          filename = "__base__/graphics/entity/gate/gate-rail-horizontal-shadow-left.png",
          line_length = 8,
          width = 122,
          height = 60,
          frame_count = 16,
          shift = util.by_pixel(12, 10),
          draw_as_shadow = true,
          scale = 0.5
        }
      }
    },
    horizontal_rail_animation_right =
    {
      layers =
      {
        {
          filename = "__base__/graphics/entity/gate/gate-rail-horizontal-right.png",
          line_length = 8,
          width = 66,
          height = 74,
          frame_count = 16,
          shift = util.by_pixel(0, -7),
          scale = 0.5
        },
        {
          filename = "__base__/graphics/entity/gate/gate-rail-horizontal-shadow-right.png",
          line_length = 8,
          width = 122,
          height = 58,
          frame_count = 16,
          shift = util.by_pixel(12, 11),
          draw_as_shadow = true,
          scale = 0.5
        }
      }
    },
    vertical_rail_animation_left =
    {
      layers =
      {
        {
          filename = "__base__/graphics/entity/gate/gate-rail-vertical-left.png",
          line_length = 8,
          width = 42,
          height = 118,
          frame_count = 16,
          shift = util.by_pixel(0, -13),
          scale = 0.5
        },
        {
          filename = "__base__/graphics/entity/gate/gate-rail-vertical-shadow-left.png",
          line_length = 8,
          width = 82,
          height = 104,
          frame_count = 16,
          shift = util.by_pixel(9, 9),
          draw_as_shadow = true,
          scale = 0.5
        }
      }
    },
    vertical_rail_animation_right =
    {
      layers =
      {
        {
          filename = "__base__/graphics/entity/gate/gate-rail-vertical-right.png",
          line_length = 8,
          width = 42,
          height = 118,
          frame_count = 16,
          shift = util.by_pixel(0, -13),
          scale = 0.5
        },
        {
          filename = "__base__/graphics/entity/gate/gate-rail-vertical-shadow-right.png",
          line_length = 8,
          width = 82,
          height = 104,
          frame_count = 16,
          shift = util.by_pixel(9, 9),
          draw_as_shadow = true,
          scale = 0.5
        }
      }
    },
    vertical_rail_base =
    {
      filename = "__base__/graphics/entity/gate/gate-rail-base-vertical.png",
      line_length = 8,
      width = 138,
      height = 130,
      frame_count = 16,
      shift = util.by_pixel(-1, 0),
      scale = 0.5
    },
    horizontal_rail_base =
    {
      filename = "__base__/graphics/entity/gate/gate-rail-base-horizontal.png",
      line_length = 8,
      width = 130,
      height = 104,
      frame_count = 16,
      shift = util.by_pixel(0, 3),
      scale = 0.5
    },
    wall_patch =
    {
      layers =
      {
        {
          filename = "__base__/graphics/entity/gate/gate-wall-patch.png",
          line_length = 8,
          width = 70,
          height = 94,
          frame_count = 16,
          shift = util.by_pixel(-1, 13),
          scale = 0.5
        },
        {
          filename = "__base__/graphics/entity/gate/gate-wall-patch-shadow.png",
          line_length = 8,
          width = 82,
          height = 72,
          frame_count = 16,
          shift = util.by_pixel(9, 33),
          draw_as_shadow = true,
          scale = 0.5
        }
      }
    },
    impact_category = "stone",
    opening_sound = sounds.gate_open,
    closing_sound = sounds.gate_close
  }
  apply_reaction(e, mk_level)
  return e
end

local health_mk1 = settings.startup["ic-reinforced-walls-health-mk1"].value
local health_mk2 = settings.startup["ic-reinforced-walls-health-mk2"].value
local health_mk3 = settings.startup["ic-reinforced-walls-health-mk3"].value
local health_mk4 = settings.startup["ic-reinforced-walls-health-mk4"].value

local res_mult_mk1 = settings.startup["ic-reinforced-walls-resistances-multiplier-mk1"].value
local res_mult_mk2 = settings.startup["ic-reinforced-walls-resistances-multiplier-mk2"].value
local res_mult_mk3 = settings.startup["ic-reinforced-walls-resistances-multiplier-mk3"].value
local res_mult_mk4 = settings.startup["ic-reinforced-walls-resistances-multiplier-mk4"].value

local resistances_mk1 = {
  {
    type = "physical",
    decrease = 10,
    percent = math.min(100, 20 * res_mult_mk1)
  },
  {
    type = "impact",
    decrease = 50,
    percent = math.min(100, 60 * res_mult_mk1)
  },
  {
    type = "explosion",
    decrease = 15,
    percent = math.min(100, 30 * res_mult_mk1)
  },
  {
    type = "fire",
    percent = math.min(100, 100 * res_mult_mk1)
  },
  {
    type = "acid",
    decrease = 5,
    percent = math.min(100, 80 * res_mult_mk1)
  },
  {
    type = "laser",
    decrease = 5,
    percent = math.min(100, 70 * res_mult_mk1)
  }
}
local resistances_mk2 = {
  {
    type = "physical",
    decrease = 15,
    percent = math.min(100, 20 * res_mult_mk2)
  },
  {
    type = "impact",
    decrease = 55,
    percent = math.min(100, 60 * res_mult_mk2)
  },
  {
    type = "explosion",
    decrease = 20,
    percent = math.min(100, 30 * res_mult_mk2)
  },
  {
    type = "fire",
    percent = math.min(100, 100 * res_mult_mk2)
  },
  {
    type = "acid",
    decrease = 10,
    percent = math.min(100, 80 * res_mult_mk2)
  },
  {
    type = "laser",
    decrease = 10,
    percent = math.min(100, 70 * res_mult_mk2)
  }
}
local resistances_mk3 = {
  {
    type = "physical",
    decrease = 20,
    percent = math.min(100, 20 * res_mult_mk3)
  },
  {
    type = "impact",
    decrease = 60,
    percent = math.min(100, 60 * res_mult_mk3)
  },
  {
    type = "explosion",
    decrease = 25,
    percent = math.min(100, 30 * res_mult_mk3)
  },
  {
    type = "fire",
    percent = math.min(100, 100 * res_mult_mk3)
  },
  {
    type = "acid",
    decrease = 15,
    percent = math.min(100, 80 * res_mult_mk3)
  },
  {
    type = "laser",
    decrease = 15,
    percent = math.min(100, 70 * res_mult_mk3)
  }
}
local resistances_mk4 = {
  {
    type = "physical",
    decrease = 30,
    percent = math.min(100, 20 * res_mult_mk4)
  },
  {
    type = "impact",
    decrease = 70,
    percent = math.min(100, 60 * res_mult_mk4)
  },
  {
    type = "explosion",
    decrease = 35,
    percent = math.min(100, 30 * res_mult_mk4)
  },
  {
    type = "fire",
    percent = math.min(100, 100 * res_mult_mk4)
  },
  {
    type = "acid",
    decrease = 25,
    percent = math.min(100, 80 * res_mult_mk4)
  },
  {
    type = "laser",
    decrease = 25,
    percent = math.min(100, 70 * res_mult_mk4)
  }
}

local wall = data.raw["wall"]["stone-wall"]
wall.next_upgrade = "ic-reinforced-wall-mk1"

local gate = data.raw["gate"]["gate"]
gate.next_upgrade = "ic-reinforced-gate-mk1"

data:extend(
  {
    create_wall(1, health_mk1, resistances_mk1, "ic-reinforced-wall-mk2"),
    create_wall(2, health_mk2, resistances_mk2, "ic-reinforced-wall-mk3"),
    create_wall(3, health_mk3, resistances_mk3, "ic-reinforced-wall-mk4"),
    create_wall(4, health_mk4, resistances_mk4, nil),

    create_gate(1, health_mk1, resistances_mk1, "ic-reinforced-gate-mk2"),
    create_gate(2, health_mk2, resistances_mk2, "ic-reinforced-gate-mk3"),
    create_gate(3, health_mk3, resistances_mk3, "ic-reinforced-gate-mk4"),
    create_gate(4, health_mk4, resistances_mk4, nil)
  }
)
