local function create_wall(mk_level, in_ingredients)
  return {
    type = "recipe",
    name = "ic-reinforced-wall-mk" .. mk_level,
    enabled = false,
    ingredients = in_ingredients,
    results = {
      {
        type = "item",
        name = "ic-reinforced-wall-mk" .. mk_level,
        amount = 1
      }
    }
  }
end

local function create_gate(mk_level, in_ingredients)
  return {
    type = "recipe",
    name = "ic-reinforced-gate-mk" .. mk_level,
    enabled = false,
    ingredients = in_ingredients,
    results = {
      {
        type = "item",
        name = "ic-reinforced-gate-mk" .. mk_level,
        amount = 1
      }
    }
  }
end

local recipe_mult = settings.startup["ic-reinforced-walls-recipe-multiplier"].value

data:extend(
  {
    create_wall(
      1,
      {
        { type = "item", name = "stone-wall",  amount = math.max(1, math.floor(recipe_mult * 2)) },
        { type = "item", name = "steel-plate", amount = math.max(1, math.floor(recipe_mult * 4)) }
      }
    ),
    create_wall(
      2,
      {
        { type = "item", name = "ic-reinforced-wall-mk1", amount = math.max(1, math.floor(recipe_mult * 2)) },
        { type = "item", name = "low-density-structure",  amount = math.max(1, math.floor(recipe_mult * 2)) }
      }
    ),
    create_wall(
      3,
      {
        { type = "item", name = "ic-reinforced-wall-mk2", amount = math.max(1, math.floor(recipe_mult * 2)) },
        { type = "item", name = "uranium-238",            amount = math.max(1, math.floor(recipe_mult * 4)) }
      }
    ),
    create_wall(
      4,
      {
        { type = "item", name = "ic-reinforced-wall-mk3", amount = math.max(1, math.floor(recipe_mult * 2)) },
        { type = "item", name = "uranium-235",            amount = math.max(1, math.floor(recipe_mult * 2)) },
        { type = "item", name = "battery",                amount = math.max(1, math.floor(recipe_mult * 6)) }
      }
    ),

    create_gate(
      1,
      {
        { type = "item", name = "gate",               amount = math.max(1, math.floor(recipe_mult * 2)) },
        { type = "item", name = "steel-plate",        amount = math.max(1, math.floor(recipe_mult * 4)) },
        { type = "item", name = "electronic-circuit", amount = math.max(1, math.floor(recipe_mult * 3)) }
      }
    ),
    create_gate(
      2,
      {
        { type = "item", name = "ic-reinforced-gate-mk1", amount = math.max(1, math.floor(recipe_mult * 2)) },
        { type = "item", name = "low-density-structure",  amount = math.max(1, math.floor(recipe_mult * 4)) },
        { type = "item", name = "advanced-circuit",       amount = math.max(1, math.floor(recipe_mult * 3)) }
      }
    ),
    create_gate(
      3,
      {
        { type = "item", name = "ic-reinforced-gate-mk2", amount = math.max(1, math.floor(recipe_mult * 2)) },
        { type = "item", name = "uranium-238",            amount = math.max(1, math.floor(recipe_mult * 8)) },
        { type = "item", name = "processing-unit",        amount = math.max(1, math.floor(recipe_mult * 3)) }
      }
    ),
    create_gate(
      4,
      {
        { type = "item", name = "ic-reinforced-gate-mk3", amount = math.max(1, math.floor(recipe_mult * 2)) },
        { type = "item", name = "uranium-235",            amount = math.max(1, math.floor(recipe_mult * 8)) },
        { type = "item", name = "battery",                amount = math.max(1, math.floor(recipe_mult * 16)) }
      }
    )
  }
)
