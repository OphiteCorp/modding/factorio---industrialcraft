data:extend({
	{
		type = "int-setting",
		name = "ic-reinforced-walls-health-mk1",
		order = "a-a",
		setting_type = "startup",
		default_value = 2500,
		minimum_value = 1,
		maximum_value = 1000000
	},
	{
		type = "int-setting",
		name = "ic-reinforced-walls-health-mk2",
		order = "a-b",
		setting_type = "startup",
		default_value = 6000,
		minimum_value = 1,
		maximum_value = 10000000
	},
	{
		type = "int-setting",
		name = "ic-reinforced-walls-health-mk3",
		order = "a-c",
		setting_type = "startup",
		default_value = 14000,
		minimum_value = 1,
		maximum_value = 100000000
	},
	{
		type = "int-setting",
		name = "ic-reinforced-walls-health-mk4",
		order = "a-d",
		setting_type = "startup",
		default_value = 30000,
		minimum_value = 1,
		maximum_value = 100000000
	},
	{
		type = "double-setting",
		name = "ic-reinforced-walls-resistances-multiplier-mk1",
		order = "b-a",
		setting_type = "startup",
		default_value = 1,
		minimum_value = 0.1,
		maximum_value = 5
	},
	{
		type = "double-setting",
		name = "ic-reinforced-walls-resistances-multiplier-mk2",
		order = "b-b",
		setting_type = "startup",
		default_value = 1.15,
		minimum_value = 0.1,
		maximum_value = 5
	},
	{
		type = "double-setting",
		name = "ic-reinforced-walls-resistances-multiplier-mk3",
		order = "b-c",
		setting_type = "startup",
		default_value = 1.3,
		minimum_value = 0.1,
		maximum_value = 5
	},
	{
		type = "double-setting",
		name = "ic-reinforced-walls-resistances-multiplier-mk4",
		order = "b-d",
		setting_type = "startup",
		default_value = 1.5,
		minimum_value = 0.1,
		maximum_value = 5
	},
	{
		type = "double-setting",
		name = "ic-reinforced-walls-recipe-multiplier",
		order = "c",
		setting_type = "startup",
		default_value = 1.0,
		minimum_value = 0.1,
		maximum_value = 1000
	},
	{
		type = "double-setting",
		name = "ic-reinforced-walls-technology-multiplier",
		order = "d",
		setting_type = "startup",
		default_value = 1.0,
		minimum_value = 0.1,
		maximum_value = 1000
	},
	{
		type = "double-setting",
		name = "ic-reinforced-walls-reflect-multiplier",
		order = "e",
		setting_type = "startup",
		default_value = 1.0,
		minimum_value = 0,
		maximum_value = 10000000
	}
})