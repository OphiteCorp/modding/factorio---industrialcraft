# Reinforced Walls

Přidá 4 nové zdi a brány, aby bylo možné odolat i silnějším nepřátelům.

- Možnost konfigurace jak životů, tak odolností

### Ocelová zeď, brána a 2 výzkumy

![](/ic-reinforced-walls/graphics/icons/ic-reinforced-wall-mk1.png){:height="96px" width="96px"}
![](/ic-reinforced-walls/graphics/icons/ic-reinforced-gate-mk1.png){:height="96px" width="96px"}
![](/ic-reinforced-walls/graphics/technology/ic-reinforced-wall-mk1.png){:height="96px" width="96px"}
![](/ic-reinforced-walls/graphics/technology/ic-reinforced-gate-mk1.png){:height="96px" width="96px"}

### Strukturovaná zeď, brána a 2 výzkumy

![](/ic-reinforced-walls/graphics/icons/ic-reinforced-wall-mk2.png){:height="96px" width="96px"}
![](/ic-reinforced-walls/graphics/icons/ic-reinforced-gate-mk2.png){:height="96px" width="96px"}
![](/ic-reinforced-walls/graphics/technology/ic-reinforced-wall-mk2.png){:height="96px" width="96px"}
![](/ic-reinforced-walls/graphics/technology/ic-reinforced-gate-mk2.png){:height="96px" width="96px"}

### Uranová zeď, brána a 2 výzkumy

![](/ic-reinforced-walls/graphics/icons/ic-reinforced-wall-mk3.png){:height="96px" width="96px"}
![](/ic-reinforced-walls/graphics/icons/ic-reinforced-gate-mk3.png){:height="96px" width="96px"}
![](/ic-reinforced-walls/graphics/technology/ic-reinforced-wall-mk3.png){:height="96px" width="96px"}
![](/ic-reinforced-walls/graphics/technology/ic-reinforced-gate-mk3.png){:height="96px" width="96px"}

### Uranová obohacená zeď, brána a 2 výzkumy

![](/ic-reinforced-walls/graphics/icons/ic-reinforced-wall-mk4.png){:height="96px" width="96px"}
![](/ic-reinforced-walls/graphics/icons/ic-reinforced-gate-mk4.png){:height="96px" width="96px"}
![](/ic-reinforced-walls/graphics/technology/ic-reinforced-wall-mk4.png){:height="96px" width="96px"}
![](/ic-reinforced-walls/graphics/technology/ic-reinforced-gate-mk4.png){:height="96px" width="96px"}
