# Industrial Craft

**Módy do hry Factorio** ve verzi **2.0**
- Vytvořeny na verzi hry 2.0.22+ (měly by "snad" fungovat na celé verzi 2.0)
- Některé mohou vyžadovat DLC "Space-Age" s podporou vesmíru kvalit

### Mód - [**Reinforced Walls**](/ic-reinforced-walls/)

Přidá několik nových zdí a bran, aby bylo možné odolat i silnějším nepřátelům.

### Mód - [**More Qualities**](/ic-more-qualities/)

Přidá 40+ nových kvalit.

### Mód - [**Tweaks**](/ic-tweaks/)

Různá nastavení, které se nemusí týkat žádného konkrétního módu.\
Například velikost inventáře, velikost stacků předmětů, počet modul slotů a jejich nastavení atd.

### Mód - [**Quality Converter**](/ic-quality-converter/)

Truhly, které mění kvalitu předmětů (snižují i zvyšují).
