# More Qualities

Přidá další kvality do hry.

- Obsahuje 3 režimy:
  1) Přidá všechny dostupné kvality (40)
  2) Přidá pár kvalit za legendární kvalitu (9) - ponechá původní ikonky (starý styl)
  3) Přidá pár kvalit za legendární kvalitu (10) - nahradí všechny ikonky (nový styl)
- Možnost změnit efektivitu nebo šanci na vyšší kvalitu pomocí konfigurace
- Možnost změnit dynamickou šanci na lepší kvalitu, kde každý lepší tier kvality získává o něco lepší šanci na další tier
- Všechny kvality se odemykají přes moduly kvality 1-3

### Seznam nových kvalit

![](/ic-more-qualities/graphics/icons/quality-uncommon-t1.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-uncommon-t2.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-uncommon-t3.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-uncommon-t4.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-uncommon-t5.png){:height="64px" width="64px"}

![](/ic-more-qualities/graphics/icons/quality-rare-t1.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-rare-t2.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-rare-t3.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-rare-t4.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-rare-t5.png){:height="64px" width="64px"}

![](/ic-more-qualities/graphics/icons/quality-epic-t1.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-epic-t2.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-epic-t3.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-epic-t4.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-epic-t5.png){:height="64px" width="64px"}

![](/ic-more-qualities/graphics/icons/quality-legendary-t1.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-legendary-t2.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-legendary-t3.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-legendary-t4.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-legendary-t5.png){:height="64px" width="64px"}

![](/ic-more-qualities/graphics/icons/quality-unique-t1.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-unique-t2.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-unique-t3.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-unique-t4.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-unique-t5.png){:height="64px" width="64px"}

![](/ic-more-qualities/graphics/icons/quality-astral-t1.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-astral-t2.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-astral-t3.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-astral-t4.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-astral-t5.png){:height="64px" width="64px"}

![](/ic-more-qualities/graphics/icons/quality-quantum-t1.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-quantum-t2.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-quantum-t3.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-quantum-t4.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-quantum-t5.png){:height="64px" width="64px"}

![](/ic-more-qualities/graphics/icons/quality-infernal-t1.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-infernal-t2-v1.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-circle-t1.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-extreme.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-spiral-t2.png){:height="64px" width="64px"}

![](/ic-more-qualities/graphics/icons/quality-rect-normal.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-rect-t1.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-rect-t2.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-rect-t3.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-rect-t4.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-rect-t5.png){:height="64px" width="64px"}
![](/ic-more-qualities/graphics/icons/quality-rect-t6.png){:height="64px" width="64px"}
