data:extend
({
    {
        type = "technology",
        name = "ic-ultimate-quality",
        icon = "__ic-more-qualities__/graphics/technology/ultimate-quality.png",
        icon_size = 256,
        effects = {},
        prerequisites = { "promethium-science-pack", "legendary-quality" },
        unit =
        {
            count = 5000,
            ingredients =
            {
                { "automation-science-pack",      1 },
                { "logistic-science-pack",        1 },
                { "military-science-pack",        1 },
                { "chemical-science-pack",        1 },
                { "production-science-pack",      1 },
                { "utility-science-pack",         1 },
                { "space-science-pack",           1 },
                { "metallurgic-science-pack",     1 },
                { "electromagnetic-science-pack", 1 },
                { "agricultural-science-pack",    1 },
                { "cryogenic-science-pack",       1 },
                { "promethium-science-pack",      1 }
            },
            time = 60
        }
    }
})
