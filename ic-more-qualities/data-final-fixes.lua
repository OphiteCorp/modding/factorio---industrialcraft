require("prototypes.utils")
require("prototypes.remove-tech-final")

local conf_early_quality = settings.startup["ic-more-qualities-early-quality"].value

if conf_early_quality then
    require("prototypes.early-quality-final")
end