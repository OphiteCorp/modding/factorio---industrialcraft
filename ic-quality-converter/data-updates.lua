ic = {
  conf = {
    upgrader_enabled = settings.startup["ic-quality-converter-upgrader"].value
  }
}

require("prototypes.data")

local quality_module_1 = data.raw.technology["quality-module"].effects

table.insert(quality_module_1, { type = "unlock-recipe", recipe = "ic-quality-converter-output-down" })
table.insert(quality_module_1, { type = "unlock-recipe", recipe = "ic-quality-converter-input-down" })

if ic.conf.upgrader_enabled then
  table.insert(quality_module_1, { type = "unlock-recipe", recipe = "ic-quality-converter-output-up" })
  table.insert(quality_module_1, { type = "unlock-recipe", recipe = "ic-quality-converter-input-up" })
end
