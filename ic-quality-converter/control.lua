if script.active_mods["gvv"] then require("__gvv__.gvv")() end

local conf_always_max = settings.startup["ic-quality-converter-always-max"].value
local conf_chance_for_upgrade = settings.startup["ic-quality-converter-upgrader-chance"].value

local quality_code_index = {}
local quality_index_code = {}
local first_quality_index = nil
local last_quality_index = nil
local down_exclude_list = {
  ["tank"] = true,
  ["spidertron"] = true,
  ["modular-armor"] = true,
  ["power-armor"] = true,
  ["power-armor-mk2"] = true,
  ["mech-armor"] = true
}

local entity_filter = {
  { filter = "name", name = "ic-quality-converter-output-down" },
  { filter = "name", name = "ic-quality-converter-output-up" },
  { filter = "name", name = "ic-quality-converter-input-down" },
  { filter = "name", name = "ic-quality-converter-input-up" }
}

-- vytvori mapu vsech kvalit, protoze to zvysi vykon v tick metode

local i = 0
for _, quality in pairs(prototypes.quality) do
  quality_code_index[quality.name] = i
  quality_index_code[i] = quality.name
  i = i + 1
end
first_quality_index = 0
last_quality_index = i - 2

-- dodatecne funkce

local function get_previous_quality(quality_name)
  local index = quality_code_index[quality_name]
  index = math.max(first_quality_index, index - 1)
  return quality_index_code[index]
end

local function get_next_quality(quality_name)
  local index = quality_code_index[quality_name]
  index = math.min(last_quality_index, index + 1)
  return quality_index_code[index]
end

local function is_quality_unlocked(quality_name)
  return game.forces.player.is_quality_unlocked(quality_name)
end

local function is_quality_unknown(quality_name)
  return quality_name == "quality-unknown"
end

local function find_last_possible_quality(next_quality)
  local last_quality = quality_index_code[last_quality_index]
  while true do
    local next = get_next_quality(next_quality)
    if not is_quality_unlocked(next) then
      return next_quality
    end
    if last_quality == next then
      return last_quality
    end
    next_quality = next
  end
end

local function get_random(table)
  local choice = nil
  local n = 0

  for _, o in pairs(table) do
    n = n + 1
    if math.random() < (1 / n) then
      choice = o
    end
  end
  return choice
end

local function try_change_and_move_to_inventory(inventory, new_inventory, item, new_item_quality)
  if new_inventory.is_full() then
    return
  end
  inventory.remove(item)
  item.quality = new_item_quality
  new_inventory.insert(item)
end

local function try_move_to_inventory(inventory, new_inventory, item)
  if new_inventory.is_full() then
    return
  end
  inventory.remove(item)
  new_inventory.insert(item)
end

-- funkce událostí

local function on_init()
  storage.quality_reducers_output_down = {}
  storage.quality_reducers_output_up = {}
  storage.quality_reducers_down = {}
  storage.quality_reducers_up = {}
end

local function on_built(evt)
  -- nutne napred vytvorit prauzdne objekty pro kazdou planetu, aby teleportace predmetu nebyla mezi planetami
  for _, surface in pairs(game.surfaces) do
    if not storage.quality_reducers_output_down[surface.name] then
      storage.quality_reducers_output_down[surface.name] = {}
    end
    if not storage.quality_reducers_output_up[surface.name] then
      storage.quality_reducers_output_up[surface.name] = {}
    end
    if not storage.quality_reducers_down[surface.name] then
      storage.quality_reducers_down[surface.name] = {}
    end
    if not storage.quality_reducers_up[surface.name] then
      storage.quality_reducers_up[surface.name] = {}
    end
  end

  local surface = game.surfaces[evt.entity.surface_index].name

  if evt.entity.name == "ic-quality-converter-output-down" then
    --game.print("Add output down: " .. evt.entity.unit_number)
    storage.quality_reducers_output_down[surface][evt.entity.unit_number] = evt.entity
  elseif evt.entity.name == "ic-quality-converter-output-up" then
    --game.print("Add output up: " .. evt.entity.unit_number)
    storage.quality_reducers_output_up[surface][evt.entity.unit_number] = evt.entity
  elseif evt.entity.name == "ic-quality-converter-input-down" then
    --game.print("Add down: " .. evt.entity.unit_number)
    storage.quality_reducers_down[surface][evt.entity.unit_number] = evt.entity
  elseif evt.entity.name == "ic-quality-converter-input-up" then
    --game.print("Add up: " .. evt.entity.unit_number)
    storage.quality_reducers_up[surface][evt.entity.unit_number] = evt.entity
  end
end

-- logika pro OUTPUT down truhly

local function tick_output_down()
  local result = {}
  local random_output_per_surface = {}

  for surface, data in pairs(storage.quality_reducers_output_down) do
    local new_map = {}

    for unit_number, entity in pairs(data) do
      if (not entity or not entity.valid) then
        --game.print("Remove output down: " .. unit_number)
      else
        new_map[unit_number] = entity
      end
    end
    result[surface] = new_map
    random_output_per_surface[surface] = get_random(new_map)
  end

  storage.quality_reducers_output_down = result
  return random_output_per_surface
end

-- logika pro OUTPUT up truhly

local function tick_output_up()
  local result = {}
  local random_output_per_surface = {}

  for surface, data in pairs(storage.quality_reducers_output_up) do
    local new_map = {}

    for unit_number, entity in pairs(data) do
      if (not entity or not entity.valid) then
        --game.print("Remove output up: " .. unit_number)
      else
        new_map[unit_number] = entity
      end
    end
    result[surface] = new_map
    random_output_per_surface[surface] = get_random(new_map)
  end

  storage.quality_reducers_output_up = result
  return random_output_per_surface
end

-- logika pro DOWN (grade) truhly, ktere SNIZUJI kvalitu

local function tick_down(output_per_surface)
  for surface, data in pairs(storage.quality_reducers_down) do
    local output = output_per_surface[surface]
    local new_map = {}

    for unit_number, entity in pairs(data) do
      if (not entity or not entity.valid) then
        --game.print("Remove down: " .. unit_number)
      else
        new_map[unit_number] = entity
      end
    end
    storage.quality_reducers_down[surface] = new_map

    if output then
      for _, entity in pairs(new_map) do
        local inventory = entity.get_output_inventory()
        local contents = inventory.get_contents()

        for _, content in pairs(contents) do
          local quality_index = quality_code_index[content.quality]
          local has_quality_flag = quality_index > first_quality_index
          local excluded_flag = down_exclude_list[content.name]
          local unknown_flag = is_quality_unknown(content.quality)

          if has_quality_flag and not excluded_flag and not unknown_flag then
            -- vychozi je vzdy normalni kvalita
            local prev_quality = quality_index_code[first_quality_index]
            -- pokud se ma kvalita snizovat postupne
            if not conf_always_max then
              prev_quality = get_previous_quality(content.quality)
            end
            -- upravi kvalitu
            try_change_and_move_to_inventory(inventory, output.get_output_inventory(), content, prev_quality)
          else
            -- pokud se nesplni podminka na zmenu kvality, tak se pouze presune
            try_move_to_inventory(inventory, output.get_output_inventory(), content)
          end
        end
      end
    end
  end
end

-- logika pro UP (grade) truhly, ktere ZVYSUJI kvalitu

local function tick_up(output_per_surface)
  for surface, data in pairs(storage.quality_reducers_up) do
    local output = output_per_surface[surface]
    local new_map = {}

    for unit_number, entity in pairs(data) do
      if (not entity or not entity.valid) then
        --game.print("Remove up: " .. unit_number)
      else
        new_map[unit_number] = entity
      end
    end
    storage.quality_reducers_up[surface] = new_map

    if output then
      for _, entity in pairs(new_map) do
        local inventory = entity.get_output_inventory()
        local contents = inventory.get_contents()

        for _, content in pairs(contents) do
          local destroy_item = false
          if conf_chance_for_upgrade < 100 then
            destroy_item = math.random(0, 100) >= conf_chance_for_upgrade
          end
          local next_quality = get_next_quality(content.quality)
          local has_max_quality_flag = quality_index_code[last_quality_index] == content.quality
          local unlocked_flag = is_quality_unlocked(next_quality)
          local unknown_flag = is_quality_unknown(content.quality)

          if not has_max_quality_flag and unlocked_flag and not unknown_flag then
            if conf_always_max then
              -- pokud se ma vzdy zvysit na posledni kvalitu
              next_quality = find_last_possible_quality(next_quality)
              try_change_and_move_to_inventory(inventory, output.get_output_inventory(), content, next_quality)
            else
              -- pokud se item ma znicit, tak se pouze znici
              if destroy_item then
                inventory.remove(content)
              else
                -- pokud se nema znicit, tak se zkusi vylepsit o 1 tier nahoru
                try_change_and_move_to_inventory(inventory, output.get_output_inventory(), content, next_quality)
              end
            end
          else
            -- pokud se nesplni podminka na zmenu kvality, tak se pouze presune
            try_move_to_inventory(inventory, output.get_output_inventory(), content)
          end
        end
      end
    end
  end
end

local function on_tick()
  -- vystupy obsahuji mapu, kde klicem je nazev planety a hodnotou nahodna vystupni truhla

  local down_output = tick_output_down()
  tick_down(down_output)

  local up_output = tick_output_up()
  tick_up(up_output)
end

local function on_gui(evt, opened)
  local player_index = evt.player_index
  local player = game.get_player(player_index)
  local entity_gui_name = evt.entity.name .. "-" .. "-gui"

  if opened then
    local gui = player.gui.relative.add({
      type = "frame",
      name = entity_gui_name,
      caption = { "gui.control" },
      direction = "vertical",
      index = 0,
      anchor = {
        gui = defines.relative_gui_type.container_gui,
        position = defines.relative_gui_position.right
      }
    })
    local gui_inside = gui.add({
      type = "frame",
      name = entity_gui_name .. "-inside",
      style = "inside_shallow_frame_with_padding",
      direction = "vertical"
    })
    local gui_flow = gui_inside.add({
      type = "flow",
      name = entity_gui_name .. "-inside-flow",
      style = "player_input_horizontal_flow"
    })
    gui_flow.add({
      type = "label",
      name = entity_gui_name .. "-inside-flow-linkid",
      caption = { "gui.linkid" }
    })
    gui_flow.add({
      type = "choose-elem-button",
      name = entity_gui_name .. "-inside-flow-linkid-button",
      style = "slot_button_in_shallow_frame",
      elem_type = "signal",
      signal = nil
    })
  else
    if (player.gui and player.gui.valid) then
      local gui = player.gui.relative[entity_gui_name]
      if (gui and gui.valid) then gui.destroy() end
    end
  end
end

local function on_gui_opened(evt)
  if not evt.entity then
    return
  end
  local valid = false
  for _, data in pairs(entity_filter) do
    if evt.entity.valid and evt.entity.name == data.name then
      valid = true
      break
    end
  end
  if valid then
    on_gui(evt, true)
  end
end

local function on_gui_closed(evt)
  if not evt.entity then
    return
  end
  local valid = false
  for _, data in pairs(entity_filter) do
    if evt.entity.valid and evt.entity.name == data.name then
      valid = true
      break
    end
  end
  if valid then
    on_gui(evt, false)
  end
end

-- definice udalosti

script.on_init(on_init)

script.on_event(defines.events.on_built_entity, on_built, entity_filter)
script.on_event(defines.events.on_robot_built_entity, on_built, entity_filter)
script.on_event(defines.events.script_raised_built, on_built, entity_filter)
script.on_event(defines.events.script_raised_revive, on_built, entity_filter)
--script.on_event(defines.events.on_gui_opened, on_gui_opened)
--script.on_event(defines.events.on_gui_closed, on_gui_closed)

if script.active_mods["space-age"] then
  script.on_event(defines.events.on_space_platform_built_entity, on_built, entity_filter)
end

script.on_nth_tick(1, on_tick)
