data:extend({
	{
		type = "bool-setting",
		name = "ic-quality-converter-upgrader",
		order = "a",
		setting_type = "startup",
		default_value = false
	},
	{
		type = "bool-setting",
		name = "ic-quality-converter-always-max",
		order = "b",
		setting_type = "startup",
		default_value = false
	},
	{
		type = "bool-setting",
		name = "ic-quality-converter-logistic-network",
		order = "c",
		setting_type = "startup",
		default_value = false
	},
	{
		type = "int-setting",
		name = "ic-quality-converter-upgrader-chance",
		order = "d",
		setting_type = "startup",
		default_value = 100,
		minimum_value = 0,
		maximum_value = 100
	},
	{
		type = "int-setting",
		name = "ic-quality-converter-price-mult",
		order = "e",
		setting_type = "startup",
		default_value = 10,
		minimum_value = 1,
		maximum_value = 1000000
	},
	{
		type = "bool-setting",
		name = "ic-quality-converter-simple-recipe",
		order = "f",
		setting_type = "startup",
		default_value = false
	}
})
