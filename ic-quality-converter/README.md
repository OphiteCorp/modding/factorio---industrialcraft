# Quality Converter

Umožní měnit kvalitu předmětů (celých stacků) pomocí 4 speciálních truhel.\
Jsou tu 2 typy truhel - pro snížení kvality a pro zvýšení. Každý typ má vstupní a výstupní truhlu.\
Předměty mezi truhlami se teleportují a truhly sdílí stejný inventář.

![](/ic-quality-converter/graphics/steel-chest.png){:height="96px" width="96px"}
