local util = require("util")

local conf_logistic_network = settings.startup["ic-quality-converter-logistic-network"].value
local conf_price_mult = settings.startup["ic-quality-converter-price-mult"].value
local conf_simple_recipe = settings.startup["ic-quality-converter-simple-recipe"].value

local function create_entity(in_name, in_icon, in_output_icon)
  local entity = table.deepcopy(data.raw["container"]["steel-chest"])
  entity.name = "ic-quality-converter-" .. in_name
  entity.icon = "__ic-quality-converter__/graphics/steel-chest-icon.png"
  entity.minable = { mining_time = 0.2, result = "ic-quality-converter-" .. in_name }
  entity.picture = {
    layers = {
      {
        filename = "__ic-quality-converter__/graphics/steel-chest.png",
        priority = "extra-high",
        width = 64,
        height = 80,
        shift = util.by_pixel(-0.25, -0.5),
        scale = 0.5
      },
      {
        filename = "__base__/graphics/entity/steel-chest/steel-chest-shadow.png",
        priority = "extra-high",
        width = 110,
        height = 46,
        shift = util.by_pixel(12.25, 8),
        draw_as_shadow = true,
        scale = 0.5
      },
      {
        filename = "__ic-quality-converter__/graphics/" .. in_icon .. ".png",
        priority = "extra-high",
        size = 64,
        scale = 0.25,
        shift = { -0.25, -0.30 }
      }
    }
  }
  -- vystupni truhla, ktera pouze prijima predmety a neupravuje kvalitu
  if in_output_icon then
    entity.inventory_size = 129

    if conf_logistic_network then
      entity.type = "logistic-container"
      entity.logistic_mode = "passive-provider"
    end

    table.insert(entity.picture.layers, {
      filename = "__ic-quality-converter__/graphics/" .. in_output_icon .. ".png",
      priority = "extra-high",
      size = 64,
      scale = 0.25,
      shift = { 0.25, -0.30 }
    })
  else
    entity.inventory_size = 29

    if conf_logistic_network then
      entity.type = "logistic-container"
      entity.logistic_mode = "storage"
      entity.max_logistic_slots = 1
    end
  end
  return entity
end

local function create_item(in_name, in_icon, in_output_icon)
  local item = table.deepcopy(data.raw["item"]["steel-chest"])
  item.name = "ic-quality-converter-" .. in_name
  item.icons = {
    {
      icon = "__ic-quality-converter__/graphics/steel-chest-icon.png",
      icon_size = 64
    },
    {
      icon = "__ic-quality-converter__/graphics/" .. in_icon .. ".png",
      icon_size = 64,
      scale = 0.25,
      shift = { -8, -8 }
    }
  }
  item.order = "icqr-" .. in_name
  item.place_result = "ic-quality-converter-" .. in_name

  if in_output_icon then
    table.insert(item.icons, {
      icon = "__ic-quality-converter__/graphics/" .. in_output_icon .. ".png",
      icon_size = 64,
      scale = 0.25,
      shift = { 8, -8 }
    })
  end
  return item
end

local function get_recipe_ingredients()
  if conf_simple_recipe then
    return {
      { type = "item", name = "steel-chest",        amount = math.floor(1 * conf_price_mult) },
      { type = "item", name = "electronic-circuit", amount = math.floor(8 * conf_price_mult) },
      { type = "item", name = "quality-module",     amount = math.floor(12 * conf_price_mult) }
    }
  else
    return {
      { type = "item", name = "steel-chest",      amount = math.floor(1 * conf_price_mult) },
      { type = "item", name = "advanced-circuit", amount = math.floor(8 * conf_price_mult) },
      { type = "item", name = "quality-module",   amount = math.floor(12 * conf_price_mult) }
    }
  end
end

data:extend({
  create_entity("input-down", "arrow-input-down", nil),
  create_entity("output-down", "arrow-input-down", "arrow-output"),

  create_item("output-down", "arrow-input-down", "arrow-output"),
  create_item("input-down", "arrow-input-down", nil),

  {
    type = "recipe",
    name = "ic-quality-converter-output-down",
    enabled = false,
    ingredients = get_recipe_ingredients(),
    energy_required = 15,
    results = { { type = "item", name = "ic-quality-converter-output-down", amount = 1 } }
  },
  {
    type = "recipe",
    name = "ic-quality-converter-input-down",
    enabled = false,
    ingredients = get_recipe_ingredients(),
    energy_required = 15,
    results = { { type = "item", name = "ic-quality-converter-input-down", amount = 1 } }
  }
})

if ic.conf.upgrader_enabled then
  data:extend({
    create_entity("input-up", "arrow-input-up", nil),
    create_entity("output-up", "arrow-input-up", "arrow-output"),

    create_item("output-up", "arrow-input-up", "arrow-output"),
    create_item("input-up", "arrow-input-up", nil),

    {
      type = "recipe",
      name = "ic-quality-converter-output-up",
      enabled = false,
      ingredients = get_recipe_ingredients(),
      energy_required = 15,
      results = { { type = "item", name = "ic-quality-converter-output-up", amount = 1 } }
    },
    {
      type = "recipe",
      name = "ic-quality-converter-input-up",
      enabled = false,
      ingredients = get_recipe_ingredients(),
      energy_required = 15,
      results = { { type = "item", name = "ic-quality-converter-input-up", amount = 1 } }
    }
  })
end
